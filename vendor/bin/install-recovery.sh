#!/vendor/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:134217728:00a609e81cbaa036dfa555f53c753259f83f3d91; then
  applypatch  \
          --patch /vendor/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:100663296:3bdfdabb521606a79d2275df1cf6242ad11c5ded \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:134217728:00a609e81cbaa036dfa555f53c753259f83f3d91 && \
      log -t recovery "Installing new oplus recovery image: succeeded" && \
      setprop ro.boot.recovery.updated true || \
      log -t recovery "Installing new oplus recovery image: failed" && \
      setprop ro.boot.recovery.updated false
else
  log -t recovery "Recovery image already installed"
  setprop ro.boot.recovery.updated true
fi
